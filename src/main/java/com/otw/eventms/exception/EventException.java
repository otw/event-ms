package com.otw.eventms.exception;


public class EventException extends Throwable {
    public EventException(String message) {
        super(message);
    }
}
