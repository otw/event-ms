package com.otw.eventms.controller;

import com.otw.eventms.dto.ConnectedEventDTO;
import com.otw.eventms.dto.EventDTO;
import com.otw.eventms.dto.EventNameDTO;
import com.otw.eventms.exception.EventException;
import com.otw.eventms.model.Event;
import com.otw.eventms.service.EventService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/event")
public class EventRestController {
    private final EventService eventService;
    private final ModelMapper modelMapper;

    public EventRestController(EventService eventService, ModelMapper modelMapper) {
        this.eventService = eventService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/getEvents")
    public ResponseEntity<List<EventDTO>> getEvents() {
        List<Event> events = eventService.findAll();
        events = events.stream().sorted(Comparator.comparing(f -> f.getStartDate())).collect(Collectors.toList());
        List<EventDTO> eventDTOS = new ArrayList<>();
        for (Event event : events) {
            eventDTOS.add(modelMapper.map(event, EventDTO.class));
        }
        return new ResponseEntity<>(eventDTOS, HttpStatus.OK);
    }

    @GetMapping("/getEvent/{eventId}")
    public ResponseEntity<EventDTO> getEvent(@PathVariable Long eventId) throws EventException {
        Event event = eventService.findByEventId(eventId);
        return new ResponseEntity<>(modelMapper.map(event, EventDTO.class), HttpStatus.OK);
    }

    @GetMapping("/connectedEvents")
    ResponseEntity<List<ConnectedEventDTO>> getConnectedEvents(@RequestParam(name = "eventids") List<String> eventids) throws EventException {
        List<Event> events = new ArrayList<>();
        for (String eventId : eventids) {
            Event event = eventService.findById(Long.parseLong(eventId));
            events.add(event);
        }

        List<ConnectedEventDTO> eventDtoList = new ArrayList<>();

        for (Event event : events) {
            eventDtoList.add(modelMapper.map(event, ConnectedEventDTO.class));
        }

        return new ResponseEntity(eventDtoList, HttpStatus.OK);
    }

    @GetMapping("/connectedEventNames")
    ResponseEntity<List<EventNameDTO>> getConnectedNames(@RequestParam(name = "eventids") List<Long> eventids) throws EventException {
        List<EventNameDTO> eventNameDTOS = new ArrayList<>();
        for (Long id : eventids) {
            Event event = eventService.findById(id);
            eventNameDTOS.add(new EventNameDTO(id, event.getTitle()));
        }
        return new ResponseEntity<>(eventNameDTOS, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<EventDTO> createEvent(@RequestHeader("User-ID") Long userId, @RequestBody EventDTO eventDTO) {
        Event event = modelMapper.map(eventDTO, Event.class);
        Event eventOut = eventService.saveNewEvent(userId, event);
        return new ResponseEntity<>(modelMapper.map(eventOut, EventDTO.class), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<EventDTO> updateEvent(@PathVariable Long id, @RequestBody EventDTO eventDTO) {
        Event event = eventService.update(id, eventDTO.getTitle(), eventDTO.getDescription(), eventDTO.getLatitude(), eventDTO.getLongitude(), eventDTO.getAddress(), eventDTO.getStartDate(), eventDTO.getEndDate(), eventDTO.getOrganiserId());
        return new ResponseEntity<>(modelMapper.map(event, EventDTO.class), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<EventDTO> deleteEvent(@PathVariable Long id) {
        eventService.deleteEvent(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
