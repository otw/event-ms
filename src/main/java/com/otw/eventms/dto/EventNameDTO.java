package com.otw.eventms.dto;

public class EventNameDTO {
    private Long nodeId;
    private String title;

    public EventNameDTO(){}

    public EventNameDTO(Long nodeId, String title) {
        this.nodeId = nodeId;
        this.title = title;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
