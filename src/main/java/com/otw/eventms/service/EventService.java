package com.otw.eventms.service;

import com.otw.eventms.exception.EventException;
import com.otw.eventms.model.Event;
import com.otw.eventms.repository.EventRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class EventService {
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }


    public Event findByEventId(Long id) throws EventException {
        if (eventRepository.findById(id).isPresent()) {
            return eventRepository.findById(id).get();
        } else {
            throw new EventException("Event not found!");
        }
    }

    public Event saveNewEvent(Long organiserId, Event event) {
        Event eventOut = eventRepository.save(event);
        eventOut.setOrganiserId(organiserId);
        return eventRepository.save(eventOut);
    }

    public Event save(Event event) {
        return eventRepository.save(event);
    }

    public Event update(Long id, String title, String description, Double latitude, Double longitude, String address, LocalDateTime startDate, LocalDateTime endDate, Long organisatorId) {
        Event event = eventRepository.getOne(id);
        event.setTitle(title);
        event.setDescription(description);
        event.setLatitude(latitude);
        event.setLongitude(longitude);
        event.setAddress(address);
        event.setStartDate(startDate);
        event.setEndDate(endDate);
        return this.save(event);
    }

    public void deleteEvent(Long id) {
        eventRepository.deleteById(id);
    }

    public Event findById(long eventId) throws EventException {
        Optional<Event> event = eventRepository.findById(eventId);
        if (event.isPresent()) {
            return event.get();
        }

        throw new EventException("Event not found!");
    }

    public void deleteAll() {
        eventRepository.deleteAll();
    }

    public List<Event> findAll() {
        return eventRepository.findAll();
    }
}

