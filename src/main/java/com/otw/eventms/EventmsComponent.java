package com.otw.eventms;

import com.otw.eventms.model.Event;
import com.otw.eventms.service.EventService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class EventmsComponent implements CommandLineRunner {
    private EventService eventService;

    public EventmsComponent(EventService eventService) {
        this.eventService = eventService;
    }


    @Override
    public void run(String... args) throws Exception {
        eventService.deleteAll();
        Event event = new Event("Discord met de boys", "Gewoon een beetje discorden met de boys", 51.218368, 4.400892, "Nationalestraat 5, 2000 Antwerpen", LocalDateTime.now(), LocalDateTime.now().plusDays(1L), 1L, 20);
        Event event2 = new Event("kdG Stagemoment", "Beetje netwerken met deze fantastische app", 51.218368, 4.400892, "Nationalestraat 5, 2000 Antwerpen", LocalDateTime.now(), LocalDateTime.now().plusDays(1L), 1L, 20);
        eventService.save(event);
        eventService.save(event2);
    }
}
